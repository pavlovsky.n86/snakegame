// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Block.h"
#include "Food.h"
#include "Bonus.h"
#include "Components/InputComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NumberOfCellsX = 20;
	NumberOfCellsY = 20;
	Points = 0;
		
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	
	minX = -((NumberOfCellsX - (NumberOfCellsX % 2)) / 2);
	maxX = ((NumberOfCellsX - (NumberOfCellsX % 2)) / 2);
	minY = -((NumberOfCellsY - (NumberOfCellsY % 2)) / 2);
	maxY = ((NumberOfCellsY - (NumberOfCellsY % 2)) / 2);
	Step = SnakeActor->ElementSize;
	
	AddNewFood();
	CreateFloor();
	CreateWalls();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!IsValid(FoodActor))
	{
		AddNewFood();
		Points += 10;
		if (!(Points % 100))
		{
			SnakeActor->SetActorTickInterval(SnakeActor->MovementSpeed -= 0.05);
		}

		int Rand = FMath::FRandRange(1, 100);
		if (Rand < 50 && !IsValid(BonusActor))
		{
			FVector BonusLocation(RandomSpawn());
			FTransform BonusTransform(BonusLocation);
			BonusActor = GetWorld()->SpawnActor<ABonus>(BonusActorClass, BonusTransform);
			CheckLocation(BonusActor);
		}
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateFloor()
{
	FTransform FloorTransform(FRotator(0, 0, 0), FVector(0, 0, -50), FVector((NumberOfCellsX+3) * (0.01* Step), (NumberOfCellsY+3) * (0.01 * Step), 0.2));
	ABlock* Floor = GetWorld()->SpawnActor<ABlock>(BlockClass, FloorTransform);
	//Floor->BlockMesh->SetMaterial();
}

void APlayerPawnBase::CreateWalls()
{
	float SizeBlock = 0.01 * Step;
	FTransform LeftWallTransform(FRotator(0, 0, 0), FVector(0, (-(NumberOfCellsY + 2)/2) * Step, 0), FVector((NumberOfCellsX + 3) * SizeBlock, SizeBlock, SizeBlock));
	ABlock* LeftWall = GetWorld()->SpawnActor<ABlock>(BlockClass, LeftWallTransform);
	FTransform RightWallTransform(FRotator(0, 0, 0), FVector(0, ((NumberOfCellsY + 2) / 2) * Step, 0), FVector((NumberOfCellsX + 3) * SizeBlock, SizeBlock, SizeBlock));
	ABlock* RightWall = GetWorld()->SpawnActor<ABlock>(BlockClass, RightWallTransform);
	FTransform DownWallTransform(FRotator(0, 0, 0), FVector((-(NumberOfCellsX + 2) / 2) * Step, 0, 0), FVector(SizeBlock, (NumberOfCellsY + 3) * SizeBlock, SizeBlock));
	ABlock* DownWall = GetWorld()->SpawnActor<ABlock>(BlockClass, DownWallTransform);
	FTransform UpWallTransform(FRotator(0, 0, 0), FVector(((NumberOfCellsX + 2) / 2) * Step, 0, 0), FVector(SizeBlock, (NumberOfCellsY + 3) * SizeBlock, SizeBlock));
	ABlock* UpWall = GetWorld()->SpawnActor<ABlock>(BlockClass, UpWallTransform);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}


void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN && SnakeActor->EndLastMove)
	{
		SnakeActor->LastMoveDirection = EMovementDirection::UP;
		SnakeActor->EndLastMove = false;
	}
	else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP && SnakeActor->EndLastMove)
	{
		SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		SnakeActor->EndLastMove = false;
	}
	
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT && SnakeActor->EndLastMove)
	{
		SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		SnakeActor->EndLastMove = false;
	}
	else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT && SnakeActor->EndLastMove)
	{
		SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		SnakeActor->EndLastMove = false;
	}
	
}

void APlayerPawnBase::AddNewFood()
{
		
	FTransform Transform(RandomSpawn());
	FoodActor = GetWorld()->SpawnActor<AFood>(FoodClass, Transform);
	CheckLocation(FoodActor);
}

FVector APlayerPawnBase::RandomSpawn()
{
	int SpawnX = FMath::FRandRange(minX, maxX);
	int SpawnY = FMath::FRandRange(minY, maxY);

	FVector SpawnPoint((SpawnX * Step), (SpawnY * Step), 0);

	return SpawnPoint;
}

void APlayerPawnBase::CheckLocation(AActor* Actor)
{
	for (int i = 0; i < SnakeActor->SnakeElements.Num(); ++i)
	{
		if (Actor->GetActorLocation() == SnakeActor->SnakeElements[i]->GetActorLocation())
		{
			Actor->SetActorLocation(RandomSpawn());
			i = 0;
		}
	}
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class ABlock;
class ABonus;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();
	UPROPERTY(EditDefaultsOnly)
		int32 NumberOfCellsX;
	UPROPERTY(EditDefaultsOnly)
		int32 NumberOfCellsY;
	
	int32 Points;
	int32 Step;

	int minX, maxX, minY, maxY;
	
	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;
	
	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	UPROPERTY(BlueprintReadWrite)
		ABonus* BonusActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonus> BonusActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlock> BlockClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

		
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
		void CreateFloor();
	
	UFUNCTION(BlueprintCallable)
		void CreateWalls();

	void CreateSnakeActor();
	FVector RandomSpawn();
	void CheckLocation(AActor* Actor);

	UFUNCTION(BlueprintCallable)
	void AddNewFood();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);

	

	
};
